The folder lapack-3.8.0 contains LAPACK version 3.8.0, downloaded from www.netlib.org/lapack on January 21, 2019, the only change being an updated version requirement for CMake.
The only additions are the other files in this folder, the test folder, the pre-compiled binaries for Visual Studio, and the blas library included as git submodule (in the dep folder).
For licensing information, see file Credit.txt.
