 /**
 * @file test.cpp
 * 
 * @brief Test file for lapack.
 * 
 * ==============================================================================\n
 * © Aachener Verfahrenstechnik-Systemverfahrenstechnik, RWTH Aachen University  \n
 * ==============================================================================\n
 * 
 *
 * @author Dominik Bongartz, Jaromil Najman, Alexander Mitsos
 * @date 29.09.2018
 * 
 */

#include <iostream>
#include "lapackNameMangling.h"
#define dgesv FCLAPACK_GLOBAL(dgesv,DGESV)


extern "C" {
	void dgesv(const int&, const int&, double*, const int&, int*, double*, const int&, int&);
}


int main() {

	int N = 2;
	int NRHS = 2;
	double A[4];
	int LDA = 2;
	int IPIV[2];
	double B[4];
	int LDB = 2;
	int INFO = -42;
	
	A[0] = 2.;
	A[1] = 1.;
	A[2] = -0.5;
	A[3] = 3.;
	
	B[0] = 1.;
	B[1] = 0.;
	B[2] = 0.;
	B[3] = 1.;
	
	std::cout << std::endl << "A = " << std::endl;
	std::cout << "( " << A[0] << "  " << A[2] << std::endl;
	std::cout << "  " << A[1] << "  " << A[3] << " )" << std::endl;
	std::cout << std::endl;
	
	
	
	dgesv(N,NRHS,A,LDA,IPIV,B,LDB,INFO);
	if (INFO != 0) {
		std::cout << "Something went wrong in dgesv. Return code: " << INFO << std::endl << std::endl;
	} else {
		std::cout << std::endl << "A^-1 = " << std::endl;
		std::cout << "( " << B[0] << "  " << B[2] << std::endl;
		std::cout << "  " << B[1] << "  " << B[3] << " )" << std::endl;
		std::cout << std::endl;
	}
	
	return 0;
	
}